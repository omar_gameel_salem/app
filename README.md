# README #

Sample Node.js/Express REST API.

### Contents ###

* Logging
* Swagger
* Mysql connection
* Unit tests
* Environment profiles
* Global error handling
* JWT token authentication

### How do I get set up? ###

* npm install
* npm test
* nodemon server.js