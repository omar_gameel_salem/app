var express = require('express'),
    app = express(),
    port = process.env.PORT || 3000,
    bodyParser = require('body-parser');

var authenticateService = require('./api/security/authenticateService');
var routes = require('./api/config/routes');


app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

//security filter
app.use(function (req, res, next) {
    const token = req.header('Authorization');
    authenticateService
        .authenticateAsync(req.url, token)
        .then(authenticate => {
            if (authenticate) {
                next();
            } else {
                res.status(401).json({'code': 4321, 'desc': "invalid token"});
            }
        });
});

//register routes
for (var r in routes) {
    app.use(r, routes[r]);
}

//error handler
app.use(function (err, req, res, next) {
    if (!err) {
        return next();
    }
    if (err.type && err.type === 'client') {
        res.status(400).json({'code': 4321, 'desc': err.message});
    }
    else {
        res.status(500).json({'code': 1234, 'desc': '500: Internal server error' + err});
    }
});

// swagger
var swaggerUi = require('swagger-ui-express');
var swaggerDocument = require('./swagger.json');
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.listen(port);


console.log('server started on: ' + port);
