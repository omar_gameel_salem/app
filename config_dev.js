var config = require('./config_common.js');
config.db = {
    hostname: 'localhost',
    user: 'root',
    password: 'P@ssw0rd',
    database: 'mao'
};
config.jwt = {secret: 'secretkey'};

module.exports = config;