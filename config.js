var env = process.env.NODE_ENV || 'dev'
    , cfg = require('./config_'+env);

module.exports = cfg;