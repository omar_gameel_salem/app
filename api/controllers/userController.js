'use strict';

var express = require('express');
var router = express.Router();
var tokenService = require('../security/tokenService');


router.post('/log-in', function (req, res) {
    const token = tokenService.createToken();
    res
        .status(201)
        .set('Authorization', token)
        .send();
});

module.exports = router;
