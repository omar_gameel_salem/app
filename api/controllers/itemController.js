'use strict';

var express = require('express');
var router = express.Router();
const itemService = require('../../api/services/itemService.js');

router.get('/', function (req, res) {
    itemService
        .getAll()
        .then(data => {
            res.status(200).json(data);
        });
});

router.get('/:id', function (req, res) {
    itemService
        .getById(req.params.id)
        .then(data => {
            res.status(200).json(data);
        });
});

router.post('/', function (req, res) {
    var obj = itemService.create('koko', 'lolo');
    res.status(201).json(obj);
});

module.exports = router;
