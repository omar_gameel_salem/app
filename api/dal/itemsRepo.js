const baseRepo = require('../dal/baseRepo');

function create(name, description) {
    return new item(name, description);
}

function getAll() {
    const sql = "SELECT * FROM places";
    return baseRepo.executeAsync(sql, []);
}

function getById(id) {
    const sql = "SELECT * FROM places WHERE id=?";
    return baseRepo.executeAsync(sql, [id]);
}

module.exports.create = create;
module.exports.getAll = getAll;
module.exports.getById = getById;