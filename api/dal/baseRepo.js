const pool = require('../config/db.js');

function executeQuery(sql, params, callback) {
    pool.getConnection(function (err, connection) {
        if (err) {
            throw err;
        }

        console.log('connected as id ' + connection.threadId);

        connection.query(sql, params, function (err, rows) {
            connection.release();
            if (!err) {
                callback(rows);
            } else {
                throw err;
            }
        });

        connection.on('error', function (err) {
            throw err;
        });
    });
}


function executeAsync(sql, params) {
    return new Promise((resolve, reject) => {
        executeQuery(sql, params, function (rows) {
                resolve(rows);
            }
        );
    });
}

module.exports.executeAsync = executeAsync;
