'use strict';

var item = function (name, description) {
    this.name = name;
    this.description = description;
};

module.exports = item;