"use strict";
const root = '/api/v1';

var routes = {};
routes[root + '/items'] = require('../controllers/itemController');
routes[root + '/users'] = require('../controllers/userController');

module.exports = routes;