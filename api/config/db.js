var mysql = require('mysql');

const config = require('../../config.js');

var pool = mysql.createPool({
    connectionLimit: 100, //important
    host: config.db.host,
    user: config.db.user,
    password: config.db.password,
    database: config.db.database,
    debug: false
});

module.exports = pool;