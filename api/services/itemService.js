'use strict';
const item = require('../models/item.js');
const itemsRepo = require('../dal/itemsRepo.js');

function create(name, description) {
    return new item(name, description);
}

function getAll() {
    return itemsRepo.getAll();
}

function getById(id) {
    return itemsRepo.getById(id);
}

module.exports.create = create;
module.exports.getAll = getAll;
module.exports.getById = getById;
