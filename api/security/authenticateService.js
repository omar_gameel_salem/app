"use strict";

const tokenService = require('../security/tokenService');
const list = ['/api/v1/items'];

function authenticateAsync(url, token) {
    var isProtectedPath = list.indexOf(url) > -1;
    if (!isProtectedPath) {
        return new Promise((resolve, reject) => {
            resolve(true);
        });
    }
    return tokenService
        .verifyTokenAsync(token);
}

module.exports.authenticateAsync = authenticateAsync;