"use strict";

var jwt = require('jsonwebtoken');
const config = require('../../config.js');

function createToken() {
    const params = {id: "123"};
    return jwt.sign(params, config.jwt.secret, {
        expiresIn: 86400 // expires in 24 hours
    });
}

function verifyTokenAsync(token) {
    return new Promise((resolve, reject) => {
        if (!token) {
            resolve(false);
        }
        jwt.verify(token, config.jwt.secret, function (err, decoded) {
            if (err) {
                resolve(false);
            }
            resolve(true);
        });
    });
}

module.exports.createToken = createToken;
module.exports.verifyTokenAsync = verifyTokenAsync;