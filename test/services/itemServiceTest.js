'use strict';

var expect = require("chai").expect;
const target = require('../../api/services/itemService.js');

describe('itemServiceTest', function () {

    it('getById_indexWithinRange_itemReturned', function (done) {
        //Arrange
        var id = 0;

        //Act
        var item = target.getById(id);

        //Assert
        expect(item.name).to.equal("omar");
        done();
    });

    it('getById_indexOutOfRange_exceptionThrown', function (done) {
        //Arrange
        var id = 10;

        //Act
        const f = function () {
            target.getById(id);
        };

        //Assert
        expect(f).to.throw('index out of range');
        done();
    });

});